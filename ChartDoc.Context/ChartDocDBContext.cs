﻿using ChartDoc.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace ChartDoc.Context
{
    public class ChartDocDBContext :  DbContext
    {
        public ChartDocDBContext(DbContextOptions<ChartDocDBContext> options): base(options)
        {

        }

        public DbSet<clsUser> M_USER { get; set; }

        public DbSet<ClsDoctor> M_Doctors { get; set; }

        public DbSet<clsICD> Vw_ICD { get; set; }

        public DbSet<clsDiagnosis> T_Diagnosis { get; set; }

        public DbSet<clsProcedures> T_Procedure { get; set; }

        public DbSet<clsPatientDetails> T_PatientDetails { get; set; }

        public DbSet<clsCC> Vw_ChiefComplaint { get; set; }

        public DbSet<clsAppointment> T_Appointment { get; set; }

        public DbSet<clsImpression> T_IMPRESSION { get; set; }

        public DbSet<clsDept> M_Department { get; set; }

        public DbSet<ClsPatientInsurance> T_Patient_Insurance { get; set; }

        public DbSet<ClsCalender> T_OfficeCalander { get; set; }

        public DbSet<ClsReason> M_REASON { get; set; }

        public DbSet<ClsCOPay> Vw_COPay { get; set; }

        public DbSet<clsScheduleAppointment> T_ScheduleAppointment { get; set; }

        public DbSet<ClsCPT> Vw_CPT { get; set; }

        public DbSet<ClsEncounter> Vw_Encounter_V2 { get; set; }

        public DbSet<ClsRole> tblMenuList1 { get; set; }

        public DbSet<ClsUType> M_USERTYPE { get; set; }

        public DbSet<ClsOtherPopulate> M_OTHERSPOPULATE { get; set; }

        public DbSet<ClsPatientBilling> T_Patient_Billing { get; set; }

        public DbSet<ClsPatientAuthorization> T_Patient_Authorization { get; set; }

        public DbSet<ClsPatientEmergencyContact> T_Patient_EmergencyContact { get; set; }

        public DbSet<ClsPatientEmployerContact> T_Patient_EmployerContact { get; set; }

        public DbSet<ClsPatientSocial> T_Patient_Social { get; set; }

        public DbSet<ClsService> M_SERVICE { get; set; }

        public DbSet<ClsDocument> T_DOCUMENT { get; set; }

        public DbSet<clsFollowUp> Vw_Followup { get; set; }

        public DbSet<clsObservation> Vw_Observation { get; set; }

        public DbSet<ClsAllergies> t_Allergies { get; set; }

        public DbSet<ClsImmunizations> t_Immunizations { get; set; }

        public DbSet<ClsSocials> t_Social { get; set; }

        public DbSet<ClsFamilies> t_Family { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
