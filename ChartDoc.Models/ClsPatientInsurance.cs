﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsPatientInsurance
    {
        public string PatientId { get; set; }
        public string Provider_Name { get; set; }
        //public string Provider_id { get; set; }
        [Key]
        public string Insurance_Policy { get; set; }
        public string Policy_Type { get; set; }
        //public string Policy_Type_ID { get; set; }
        public string Card_Image_FilePath { get; set; }
        public DateTime Effective_From { get; set; }
        public string Status { get; set; }
        public string Desc { get; set; }
        //public string Status_ID { get; set; }
    }
}
