﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsCOPay
    {
        public string PatientId { get; set; }
        [Key]
        public int AppointmentId { get; set; }
        public string Name { get; set; }
        public string REFNO1 { get; set; }
        public string REFNO2 { get; set; }
        public string PAYMENTDATE { get; set; }
        public decimal AMOUNT { get; set; }
        public int PaymentType { get; set; }
    }
}
