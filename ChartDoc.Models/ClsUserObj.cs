﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsUserObj
    {
        [Key]
        public int UserId { get; set; }
        public string FULLName { get; set; }
        public string DateOfBirth { get; set; }
        public string SSN { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string image { get; set; }
        public string RoleType { get; set; }
        public string RoleSubType { get; set; }
        public string specialtyType { get; set; }
        public string RoleList { get; set; }
    }
}
