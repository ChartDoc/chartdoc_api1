﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsConsultationRoom
    {
        [Key]
        public int AppointmentId { get; set; }
        public string DoctorID { get; set; }
        public string DoctorName { get; set; }
        public string PatientID { get; set; }
        public string PatientName { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        public string ColorCode { get; set; }
        public string IsReady { get; set; }
        public string Phoneno { get; set; }
        public string Email { get; set; }
        public string DateofBirth { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string ServiceID { get; set; }
        public string PositionID { get; set; }
        public string Note { get; set; }
        public string ReasonID { get; set; }
        public string Reason { get; set; }
        public string AppointmentFromTime { get; set; }
        public string AppointmentToTime { get; set; }
        public string ROOMNO { get; set; }
        public string FlowArea { get; set; }
    }
}
