﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChartDoc.Models
{
    public class clsFlowSheet
    {
        public clsScheduleAppointment schduleAppoinment { get; set; }
        public clsWaitingArea WaitingArea { get; set; }
        public clsConsultationRoom ConsultationRoom { get; set; }
        public clsCheckingOut CheckingOut { get; set; }
    }
}
