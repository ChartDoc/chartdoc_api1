﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsCPT
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public string PatientId { get; set; }
        public int AppointmentId { get; set; }
    }
}
