﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsPatientSocial
    {
        [Key]
        public string PatientId { get; set; }
        public string Marital_Status { get; set; }
        public string Guardian_FName { get; set; }
        public string Guardian_LName { get; set; }
        public string Add_Line { get; set; }
        public string Add_City { get; set; }
        public string Add_State { get; set; }
        public string Add_Zip { get; set; }
        public DateTime DOB { get; set; }
        public string Patient_SSN { get; set; }
        public string Phone_Number { get; set; }
        public string Guardian_SSN { get; set; }
        public string Drivers_License_FilePath { get; set; }
        public string Race { get; set; }
        public string Ethicity { get; set; }
        public string Language { get; set; }
        public string Comm_Mode { get; set; }
    }
}
