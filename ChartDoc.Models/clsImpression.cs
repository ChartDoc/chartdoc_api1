﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsImpression
    {
        [Key]
        public long Id { get; set; }
        public string patientId { get; set; }

        public string Description { get; set; }
    }
}
