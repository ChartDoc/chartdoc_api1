﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsEncounter
    {
        [Key]
        public int Id { get; set; }
        public string PatientID { get; set; }
        public string Patient { get; set; }
        public string Summary { get; set; }
        public int DoctorID { get; set; }
        public string EncounterDate { get; set; }
        public string DoctorName { get; set; }
        public string EncounterNote { get; set; }
    }
}
