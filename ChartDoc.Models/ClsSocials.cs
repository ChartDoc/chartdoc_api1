﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsSocials
    {
        [Key]
        public long ID { get; set; }
        public string PatientId { get; set; }
        public string Addiection { get; set; }
        public string Frequency { get; set; }
        public string Duration { get; set; }
    }
}
