﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsDocument
    {
        [Key]
        public string PatientId { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }
    }
}
