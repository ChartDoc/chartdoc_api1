﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsICD
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public string PatientId { get; set; }
        public int AppointmentId { get; set; }
    }
}
