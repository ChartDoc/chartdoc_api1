﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsAlert
    {
        [Key]
        public int ID { get; set; }
        public string PatientID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
