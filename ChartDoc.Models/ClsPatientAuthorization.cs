﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsPatientAuthorization
    {
        [Key]
        public string PatientId { get; set; }
        public string Authorization_FilePath { get; set; }
    }
}
