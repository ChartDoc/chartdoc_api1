﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsObservation
    {
        public string PatientID { get; set; }
        [Key]
        public string PId { get; set; }
        public string pBloodPressure_L { get; set; }
        public string pBloodPressure_R { get; set; }
        public string pTemperature { get; set; }
        public string pHeight_L { get; set; }
        public string pHeight_R { get; set; }
        public string pWeight { get; set; }
        public string pPulse { get; set; }
        public string pRespiratory { get; set; }
        public int AppointmentId { get; set; }
    }
}
