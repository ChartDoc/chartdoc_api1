﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsUType
    {
        [Key]
        public int UTID { get; set; }
        public string UTCODE { get; set; }
        public string UTDESCRIPTION { get; set; }
    }
}
