﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsProcedures
    {
        [Key]
        public int Id { get; set; }
        public string PatientId { get; set; }
        public DateTime ProcedureDate { get; set; }
        public string Dr_Name { get; set; }
        public string Dr_Profile { get; set; }
        public string ProcedureDesc { get; set; }
    }
}
