﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsOtherPopulate
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
    }
}
