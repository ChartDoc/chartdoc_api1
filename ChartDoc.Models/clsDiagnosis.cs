﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsDiagnosis
    {
        [Key]
        public long Id { get; set; }
        public string PatientId { get; set; }
        public DateTime DiagnosisDate { get; set; }
        public string DiagnosisDesc { get; set; }
    }
}
