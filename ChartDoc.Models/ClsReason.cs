﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsReason
    {
        [Key]
        public int REASONID { get; set; }
        public string REASONCODE { get; set; }
        public string REASONDESCRIPTOON { get; set; }
        public string Status { get; set; }
    }
}
