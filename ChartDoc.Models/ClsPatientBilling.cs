﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsPatientBilling
    {
        [Key]
        public string PatientId { get; set; }
        public string Billing_Party { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public DateTime DOB { get; set; }
        public string Add_Line { get; set; }
        public string Add_Line1 { get; set; }
        public string Add_City { get; set; }
        public string Add_State { get; set; }
        public string Add_Zip { get; set; }
        public string SSN { get; set; }
        public string Drivers_License_FilePath { get; set; }
        public string Primary_Phone { get; set; }
        public string Secondary_Phone { get; set; }
    }
}
