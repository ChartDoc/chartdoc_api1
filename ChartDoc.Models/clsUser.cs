﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChartDoc.Models
{
    public class clsUser
    {
        [Key]
        public int IUserID { get; set; }
        public string UserID { get; set; }
        public string DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string Code { get; set; }
        public int UserType { get; set; }
        public string APPLICABLETO { get; set; }
        public string FNAME { get; set; }
        public string UserName { get; set; }
        public string USERTAG { get; set; }
        public string ImagePath { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDesc { get; set; }
        public string UTNAME { get; set; }
    }
}
