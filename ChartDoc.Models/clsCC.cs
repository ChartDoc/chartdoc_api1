﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsCC
    {
        [Key]
        public string PId { get; set; }
        public string pCcDescription { get; set; }
        //public string Description { get; set; }
        public int AppointmentId { get; set; }
    }
}
