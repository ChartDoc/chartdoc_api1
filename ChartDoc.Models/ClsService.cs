﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsService
    {
        [Key]
        public int ServiceID { get; set; }
        public string ServiceName { get; set; }
    }
}
