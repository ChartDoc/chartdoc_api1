﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsPatientDetails
    {
        [Key]
        public int Id { get; set; }
        public string PatientId { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Add_Line { get; set; }
        public string Add_City { get; set; }
        public string Add_State { get; set; }
        public string Add_PostalCode { get; set; }
        public string Add_Country { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Mob_No { get; set; }
        public string Image_Name { get; set; }
        public string Image_Path { get; set; }
        public string Data_Flag { get; set; }
        public string Age { get; set; }
        public string RecopiaID { get; set; }
        public string RecopiaName { get; set; }
    }
}
