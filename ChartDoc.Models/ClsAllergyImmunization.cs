﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsAllergyImmunization
    {
        [Key]
        public string PatientID { get; set; }
        public ClsAllergies[] Allergies { get; set; }
        public ClsImmunizations[] Immunizations { get; set; }
        public ClsAlert Alert { get; set; }
        public ClsSocials[] Socials { get; set; }
        public ClsFamilies[] Families { get; set; }
    }
}
