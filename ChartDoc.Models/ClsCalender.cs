﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsCalender
    {
        [Key]
        public int Id { get; set; }
        public string TAG { get; set; }
        public string TAGDesc { get; set; }
        public string DoctorID { get; set; }
        public string Doctor { get; set; }
        public string Date { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string EventReason { get; set; }
        public int BooingTag { get; set; }
        public string BookingDesc { get; set; }
    }
}
