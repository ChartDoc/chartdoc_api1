﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsRole
    {
        [Key]
        public int ID { get; set; }
        public string PageName { get; set; }
    }
}
