﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsPatientEmergencyContact
    {
        [Key]
        public string PatientId { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string Relationship { get; set; }
    }
}
