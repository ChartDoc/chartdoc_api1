﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsAllergies
    {
        [Key]
        public long ID { get; set; }
        public string PatientId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
