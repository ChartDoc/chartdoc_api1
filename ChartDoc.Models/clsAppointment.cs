﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsAppointment
    {
        [Key]
        public int AppointmentId { get; set; }
        public string AppointmentNo { get; set; }
        public string PatientId { get; set; }
        public string PatientName { get; set; }
        public string Address { get; set; }
        public string ContactNO { get; set; }
        public string DoctorId { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonDescription { get; set; }
        public string Tag { get; set; }
        public string ReasonID { get; set; }
        public string Reason { get; set; }
        public Boolean IsReady { get; set; }
        public int PositionID { get; set; }
        public string PositionName { get; set; }
        public string RoomNO { get; set; }
        public string Flowarea { get; set; }
        public int ServiceId { get; set; }
        public string Note { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string DOB { get; set; }
        public string FilePath { get; set; }
    }
}
