﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsFamilies
    {
        [Key]
        public long Id { get; set; }
        public string PatientId { get; set; }
        public string Member { get; set; }
        public string Diseases { get; set; }
    }
}
