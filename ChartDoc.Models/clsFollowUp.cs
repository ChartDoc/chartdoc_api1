﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class clsFollowUp
    {
        public string PatientID { get; set; }
        [Key]
        public string PId { get; set; }
        public string pFollowupDate { get; set; }
        public string pFollowupPeriod { get; set; }
        public int AppointmentID { get; set; }
    }
}
