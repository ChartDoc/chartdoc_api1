﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChartDoc.Models
{
    public class clsPatientInformations
    {
        public clsPatientDetails sPatientDetails { get; set; }
        public ClsPatientAuthorization sPatientAuthorisation { get; set; }
        public ClsPatientBilling sPatientBilling { get; set; }
        public ClsPatientEmergencyContact sPatientEmergency { get; set; }
        public ClsPatientEmployerContact sPatientEmpContact { get; set; }
        public ClsPatientInsurance[] sPatientInsurance { get; set; }
        public ClsPatientSocial sPatientSocial { get; set; }
    }
}
