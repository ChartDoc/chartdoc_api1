﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChartDoc.Models
{
    public class ClsDoctor
    {
        [Key]
        public int userID { get; set; }
        public string departmentID { get; set; }
        public string departmentName { get; set; }
        public string CODE { get; set; }
        public string userType { get; set; }
        public string applicableto { get; set; }
        public string fname { get; set; }
        public string userName { get; set; }
        public string usertag { get; set; }
        public string imagePath {get; set; }
        public string doctorName { get; set; }
        public string doctorImage { get; set; }
        
    }
}
