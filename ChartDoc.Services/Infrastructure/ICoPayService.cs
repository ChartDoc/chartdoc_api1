﻿using ChartDoc.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChartDoc.Services.Infrastructure
{
    public interface ICoPayService
    {
        List<ClsCOPay> GetCOPay(string AppointmentID);
        string SaveCoPay(ClsCOPay CoPay);
    }
}
