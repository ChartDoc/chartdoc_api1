﻿using ChartDoc.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChartDoc.Services.Infrastructure
{
    public interface ICalendarService
    {
        List<ClsCalender> GetCalender();
        string SaveCalender(ClsCalender Calendr);
    }
}
