﻿using ChartDoc.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChartDoc.Services.Infrastructure
{
    public interface IUserService
    {
        clsUser GetUser(string userName, string password);
        List<clsUser> GetAllDoctorsDetails(string date);
        List<ClsDoctor> GetUserList(string userType);
        string SaveUser(string iUser);
    }
}
