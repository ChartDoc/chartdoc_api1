﻿using ChartDoc.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChartDoc.Services.Infrastructure
{
    public interface ICptService
    {
        List<ClsCPT> GetCPT();
        List<ClsCPT> GetSavedCPT(string PatientId);
        string SaveCPT(int appointmentId, string cpt);
    }
}
