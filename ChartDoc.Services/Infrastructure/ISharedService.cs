﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ChartDoc.Services.Infrastructure
{
    public interface ISharedService
    {
        string DecodeString(string encodedString);
        DataTable SingleObjToDataTable<T>(T objects);
        DataTable ObjArrayToDataTable<T>(T[] objects);
        string ConvertDatatableToXML(DataTable dt);
    }
}
