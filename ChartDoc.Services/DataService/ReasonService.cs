﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class ReasonService : IReasonService
    {
        private readonly ChartDocDBContext _context;

        public ReasonService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsReason> GetReason(string Type)
        {
            return _context.M_REASON.FromSql($"USP_FETCHREASON {Type}").ToList();
        }
    }
}
