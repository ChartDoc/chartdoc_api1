﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class UserService : IUserService
    {
        private readonly ChartDocDBContext _context;

        public UserService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<clsUser> GetAllDoctorsDetails(string date)
        {
            return _context.M_USER.FromSql($"USP_FETCH_DOCTORLIST {date}").ToList();
        }

        public clsUser GetUser(string userName, string password)
        {
            var list = _context.M_USER.FromSql($"SELECT * FROM VW_USERLOGININFO WHERE USERNAME={userName} AND PASSWORD=dbo.fnMIME64Encode(1,{password})").ToList();
            clsUser user;
            if(list.Count > 0)
            {
                user = new clsUser
                {
                    APPLICABLETO = list[0].APPLICABLETO,
                    Code = list[0].Code,
                    DepartmentID = list[0].DepartmentID,
                    DepartmentName = list[0].DepartmentName,
                    FNAME = list[0].FNAME,
                    ImagePath = list[0].ImagePath,
                    UserID = list[0].UserID,
                    UserName = list[0].UserName,
                    USERTAG = list[0].USERTAG,
                    UserType = list[0].UserType,
                    ErrorDesc = "Successful",
                    ErrorCode = "0",
                    IUserID = list[0].IUserID,
                    UTNAME = list[0].UTNAME
                };
            }
            else
            {
                user = new clsUser
                {
                    APPLICABLETO = "",
                    Code = "",
                    DepartmentID = "",
                    DepartmentName = "",
                    FNAME = "",
                    ImagePath = "",
                    UserID = "",
                    UserName = "",
                    USERTAG = "",
                    UserType = 0,
                    ErrorCode = "-2",
                    ErrorDesc = "Invalid User/Password",
                    IUserID = 0,
                    UTNAME = ""
                };
            }
            return user;
        }

        public List<ClsDoctor> GetUserList(string userType)
        {
            var userList = _context.M_Doctors.FromSql($"USP_FETCH_USERLIST {userType}").ToList();
            return userList.Where(x => !String.IsNullOrEmpty(x.userName)).ToList();
        }

        public string SaveUser(string iUser)
        {
            return _context.Database.ExecuteSqlCommand("[USP_CreateUser] @P_USERID, @P_XML",
                                                        new SqlParameter("@P_USERID", 1),
                                                        new SqlParameter("@P_XML", iUser)).ToString();


        }
    }
}
