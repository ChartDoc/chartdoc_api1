﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class PatientInformationService : IPatientInformationService
    {
        private readonly ChartDocDBContext _context;

        public PatientInformationService(ChartDocDBContext context)
        {
            _context = context;
        }

        public string CreatePatient(string patientId, string patientDetails, string patientBilling, string emergencyContact, string employerContact, string insurance, string social, string authorization)
        {
            return _context.Database.ExecuteSqlCommand("USP_CREATEUPDATE_PATIENT @PatientID," +
                                                        "@p_Details," +
                                                        "@p_Billing," +
                                                        "@p_EmergencyContact," +
                                                        "@p_EmployerContact," +
                                                        "@p_Insurance," +
                                                        "@p_Social," +
                                                        "@p_Authorization",
                                                        new SqlParameter("@PatientID", patientId),
                                                        new SqlParameter("@p_Details", patientDetails),
                                                        new SqlParameter("@p_Billing", patientBilling),
                                                        new SqlParameter("@p_EmergencyContact", emergencyContact),
                                                        new SqlParameter("@p_EmployerContact", employerContact),
                                                        new SqlParameter("@p_Insurance", insurance),
                                                        new SqlParameter("@p_Social", social),
                                                        new SqlParameter("@p_Authorization", authorization)).ToString();
        }

        public clsPatientInformations GetPatientInfo(string PatientId)
        {
            clsPatientInformations patientInformations = new clsPatientInformations();
            patientInformations.sPatientAuthorisation = GetPatientAutorizationInformation(PatientId);
            patientInformations.sPatientBilling = GetPatientBillingInformation(PatientId);
            patientInformations.sPatientDetails = GetPatientDetailsInformation(PatientId);
            patientInformations.sPatientEmergency = GetPatientEmergencyContactInformation(PatientId);
            patientInformations.sPatientEmpContact = GetPatientEmployerContactInformation(PatientId);
            patientInformations.sPatientInsurance = GetPatientInsuranceInformation(PatientId);
            patientInformations.sPatientSocial = GetPatientSocialInformation(PatientId);
            return patientInformations;
        }

        private ClsPatientAuthorization GetPatientAutorizationInformation(string patientId)
        {
            return _context.T_Patient_Authorization.Where(x => x.PatientId == patientId).FirstOrDefault();
        }

        private ClsPatientBilling GetPatientBillingInformation(string patientId)
        {
            return _context.T_Patient_Billing.Where(x => x.PatientId == patientId).FirstOrDefault();
        }

        private clsPatientDetails GetPatientDetailsInformation(string patientId)
        {
            return _context.T_PatientDetails.Where(x => x.PatientId == patientId).FirstOrDefault();
        }

        private ClsPatientEmergencyContact GetPatientEmergencyContactInformation(string patientId)
        {
            return _context.T_Patient_EmergencyContact.Where(x => x.PatientId == patientId).FirstOrDefault();
        }

        private ClsPatientEmployerContact GetPatientEmployerContactInformation(string patientId)
        {
            return _context.T_Patient_EmployerContact.Where(x => x.PatientId == patientId).FirstOrDefault();
        }

        private ClsPatientInsurance[] GetPatientInsuranceInformation(string patientId)
        {
            return _context.T_Patient_Insurance.Where(x => x.PatientId == patientId).ToArray();
        }

        private ClsPatientSocial GetPatientSocialInformation(string patientId)
        {
            return _context.T_Patient_Social.Where(x => x.PatientId == patientId).FirstOrDefault();
        }
    }
}
