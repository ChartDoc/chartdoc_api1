﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class ProcedureService : IProcedureService
    {
        private readonly ChartDocDBContext _context;

        public ProcedureService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<clsProcedures> GetProcedureByPatientId(string PatientId)
        {
            return _context.T_Procedure.Where(x => x.PatientId == PatientId).ToList();
        }
    }
}
