﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ChartDoc.Services.DataService
{
    public class OthersPopulateService : IOthersPopulateService
    {
        private readonly ChartDocDBContext _context;

        public OthersPopulateService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsOtherPopulate> OtherPopulate(string Id)
        {
            return _context.M_OTHERSPOPULATE.Where(x => x.Type == Convert.ToInt32(Id)).ToList();
        }
    }
}
