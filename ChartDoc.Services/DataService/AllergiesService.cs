﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class AllergiesService : IAllergiesService
    {
        private readonly ChartDocDBContext _context;

        public AllergiesService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsAllergies> GetAllergies(string PatientId)
        {
            List<ClsAllergies> lstAllergies = new List<ClsAllergies>();
            try
            {
                lstAllergies= _context.t_Allergies.Where(x => x.PatientId == PatientId).ToList();
            }
            catch(Exception ex)
            {

            }
            return lstAllergies;
        }
    }
}
