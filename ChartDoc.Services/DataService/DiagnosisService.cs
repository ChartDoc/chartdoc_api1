﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class DiagnosisService : IDiagnosisService
    {
        private readonly ChartDocDBContext _context;

        public DiagnosisService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<clsDiagnosis> GetDiagnosisByPatientId(string PatientId)
        {
            return _context.T_Diagnosis.Where(x => x.PatientId == PatientId).ToList();
        }
    }
}
