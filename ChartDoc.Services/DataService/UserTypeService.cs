﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class UserTypeService : IUserTypeService
    {
        private readonly ChartDocDBContext _context;

        public UserTypeService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsUType> GetUType(string Id)
        {
            return _context.M_USERTYPE.Where(x => x.UTID == Convert.ToInt32(Id)).ToList();
        }
    }
}
