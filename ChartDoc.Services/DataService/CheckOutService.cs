﻿using ChartDoc.Context;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class CheckOutService : ICheckOutService
    {
        private readonly ChartDocDBContext _context;

        public CheckOutService(ChartDocDBContext context)
        {
            _context = context;
        }

        public string UpdateCheckOut(string AppointmentID, string flg)
        {
            string result = "";
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "USP_UPDATECheckOut";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@AppointmentID", AppointmentID));
                command.Parameters.Add(new SqlParameter("@Flg", flg));
                _context.Database.OpenConnection();
                using (var res = command.ExecuteReader())
                {
                    if (res.HasRows)
                    {
                        while (res.Read())
                        {
                            result = res.GetString(0);
                        }
                    }
                }
            }
            return result;
        }
    }
}
