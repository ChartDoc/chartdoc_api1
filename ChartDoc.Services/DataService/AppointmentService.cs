﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class AppointmentService : IAppointmentService
    {
        private readonly ChartDocDBContext _context;

        public AppointmentService(ChartDocDBContext context)
        {
            _context = context;
        }

        public clsScheduleAppointment GetAppointment(string date)
        {
            throw new NotImplementedException();
        }

        public string SaveActiveAppointment(clsAppointment appt)
        {
            string res = _context.Database.ExecuteSqlCommand("USP_SAVE_APPOINTMENT @AppointmentId,@AppointmentNo," +
                                                "@PatientId,@PatientName, @Address,@ContactNO,@DoctorId," +
                                                "@Date,@FromTime,@ToTime,@ReasonCode,@ReasonDescription," +
                                                "@Tag,@ReasonID,@Reason,@IsReady,@PositionID,@PositionName," +
                                                "@RoomNO,@Flowarea,@ServiceId,@NOTE,@Gender,@Email, @DOB,@FilePath",
                                                new SqlParameter("@AppointmentId", appt.AppointmentId),
                                                new SqlParameter("@AppointmentNo", appt.AppointmentNo),
                                                new SqlParameter("@PatientId", appt.PatientId),
                                                new SqlParameter("@PatientName", appt.PatientName),
                                                new SqlParameter("@Address", appt.Address),
                                                new SqlParameter("@ContactNO", appt.ContactNO),
                                                new SqlParameter("@DoctorId", appt.DoctorId),
                                                new SqlParameter("@Date", appt.Date),
                                                new SqlParameter("@FromTime", appt.FromTime),
                                                new SqlParameter("@ToTime", appt.ToTime),
                                                new SqlParameter("@ReasonCode", appt.ReasonCode),
                                                new SqlParameter("@ReasonDescription", appt.ReasonDescription),
                                                new SqlParameter("@Tag", appt.Tag),
                                                new SqlParameter("@ReasonID", appt.ReasonID),
                                                new SqlParameter("@Reason", appt.Reason),
                                                new SqlParameter("@IsReady", appt.IsReady),
                                                new SqlParameter("@PositionID", appt.PositionID),
                                                new SqlParameter("@PositionName", appt.PositionName),
                                                new SqlParameter("@RoomNO", appt.RoomNO),
                                                new SqlParameter("@Flowarea", appt.Flowarea),
                                                new SqlParameter("@ServiceId", appt.ServiceId),
                                                new SqlParameter("@NOTE", appt.Note),
                                                new SqlParameter("@Gender", appt.Gender),
                                                new SqlParameter("@Email", appt.Email),
                                                new SqlParameter("@DOB", appt.DOB),
                                                new SqlParameter("@FilePath", appt.FilePath)).ToString();

            return res;
                                        
        }
    }
}
