﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class FollowupService : IFollowupService
    {
        private readonly ChartDocDBContext _context;

        public FollowupService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<clsFollowUp> GetFollowUp(string AppointmentId)
        {
            return _context.Vw_Followup.FromSql($"SELECT PId, PatientId, pFollowupDate, pFollowupPeriod, AppointmentID FROM Vw_Followup WHERE AppointmentID={AppointmentId}").ToList();
        }

        public string SaveFollowup(clsFollowUp objFollowup)
        {
            return _context.Database.ExecuteSqlCommand("USP_UPDATE_FollowUp_V2 @PatientId,@FollowUpDate,@FollowUpPeriod",
                                                        new SqlParameter("@PatientId", objFollowup.PatientID),
                                                        new SqlParameter("@FollowUpDate", objFollowup.pFollowupDate),
                                                        new SqlParameter("@FollowUpPeriod", objFollowup.pFollowupPeriod)).ToString();
        }
    }
}
