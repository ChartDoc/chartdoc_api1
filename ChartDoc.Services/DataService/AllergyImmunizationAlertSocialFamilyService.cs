﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class AllergyImmunizationAlertSocialFamilyService : IAllergyImmunizationAlertSocialFamilyService
    {
        private readonly ChartDocDBContext _context;

        public AllergyImmunizationAlertSocialFamilyService(ChartDocDBContext context)
        {
            _context = context;
        }

        public string SaveAllergyImmunization(string patientId, string allergies, string immunizations, string alert, string families, string socials)
        {
            return _context.Database.ExecuteSqlCommand("USP_SAVE_ALLERGIES_IMMUNIZATION_ALERT @PATIENTID,@ALLERGIESXML,@IMMUNIZATIONXML,@ALERTXML,@Family,@SOCIAL",
                                                        new SqlParameter("@PATIENTID", patientId),
                                                        new SqlParameter("@ALLERGIESXML", allergies),
                                                        new SqlParameter("@IMMUNIZATIONXML", immunizations),
                                                        new SqlParameter("@ALERTXML", alert),
                                                        new SqlParameter("@Family", families),
                                                        new SqlParameter("@SOCIAL", socials)).ToString();
        }
    }
}
