﻿using ChartDoc.Context;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class ScheduleAppointmentService : IScheduleAppointmentService
    {
        private readonly ChartDocDBContext _context;

        public ScheduleAppointmentService(ChartDocDBContext context)
        {
            _context = context;
        }

        public string UpdateMarkReady(string AppointmentID, string Flag)
        {
            return _context.Database.ExecuteSqlCommand($"USP_UPDATEMARKREADY {AppointmentID}, {Flag}").ToString();
        }
    }
}
