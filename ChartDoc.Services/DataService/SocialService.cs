﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class SocialService : ISocialService
    {
        private readonly ChartDocDBContext _context;

        public SocialService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsSocials> GetSocials(string PatientId)
        {
            return _context.t_Social.Where(x => x.PatientId == PatientId).ToList();
        }
    }
}
