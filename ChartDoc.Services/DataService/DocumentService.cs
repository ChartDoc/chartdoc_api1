﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class DocumentService : IDocumentService
    {
        private readonly ChartDocDBContext _context;

        public DocumentService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsDocument> GetDocument(string PatientId)
        {
            return _context.T_DOCUMENT.Where(x => x.PatientId == PatientId).ToList();
        }
    }
}
