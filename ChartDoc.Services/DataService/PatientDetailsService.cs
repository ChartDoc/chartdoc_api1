﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class PatientDetailsService : IPatientDetailsService
    {
        private readonly ChartDocDBContext _context;

        public PatientDetailsService(ChartDocDBContext context)
        {
            _context = context;
        }
        public List<clsPatientDetails> GetAllPatients()
        {
            return _context.T_PatientDetails.ToList();
        }

        public List<clsPatientDetails> SearchPatient(string firstName, string lastName, string DOB, string mobNo, string emailId)
        {
            if (String.IsNullOrEmpty(DOB))
            {
                return _context.T_PatientDetails.Where(x => (x.First_Name == firstName || String.IsNullOrEmpty(firstName)) && (x.Last_Name == lastName || String.IsNullOrEmpty(lastName)) && (x.Mob_No == mobNo || String.IsNullOrEmpty(mobNo)) && (x.Email == emailId || String.IsNullOrEmpty(emailId))).ToList();
            }
            else
            {
                return _context.T_PatientDetails.Where(x => (x.First_Name == firstName || String.IsNullOrEmpty(firstName)) && (x.Last_Name == lastName || String.IsNullOrEmpty(lastName)) && (x.DOB == DateTime.Parse(DOB)) && (x.Mob_No == mobNo || String.IsNullOrEmpty(mobNo)) && (x.Email == emailId || String.IsNullOrEmpty(emailId))).ToList();
            }
            
        }

        public clsPatientDetails SearchPatientbyID(string ID)
        {
            return _context.T_PatientDetails.Where(x => x.PatientId == ID).FirstOrDefault();
        }
    }
}
