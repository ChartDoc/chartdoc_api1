﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class FamilyService : IFamilyService
    {
        private readonly ChartDocDBContext _context;

        public FamilyService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsFamilies> GetFamilies(string PatientId)
        {
            return _context.t_Family.Where(x => x.PatientId == PatientId).ToList();
        }
    }
}
