﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class DepartmentService : IDepartmentService
    {
        private readonly ChartDocDBContext _context;

        public DepartmentService(ChartDocDBContext context)
        {
            _context = context;
        }
        public List<clsDept> GetDept()
        {
            return _context.M_Department.ToList();
        }
    }
}
