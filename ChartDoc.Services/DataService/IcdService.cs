﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class IcdService : IIcdService
    {
        private readonly ChartDocDBContext _context;

        public IcdService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<clsICD> GetAllICD()
        {
            return _context.Vw_ICD.FromSql("SELECT Id, Code, [Desc], PatientId, AppointmentId FROM Vw_ICD").ToList();
        }

        public List<clsICD> GetSavedICD(string PatientId)
        {
            return _context.Vw_ICD.FromSql($"SELECT Id, Code, [Desc], PatientId, AppointmentId FROM Vw_ICD WHERE PatientId='{PatientId}'").ToList();
        }

        public string SaveICD(int appointmentId, string icd)
        {
            return _context.Database.ExecuteSqlCommand("USP_SAVE_ICD_V2 @PATIENTID, @ICDXML",
                                                        new SqlParameter("@PATIENTID", appointmentId),
                                                        new SqlParameter("@ICDXML", icd)).ToString();
        }
    }
}
