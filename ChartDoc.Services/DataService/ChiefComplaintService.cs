﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class ChiefComplaintService : IChiefComplaintService
    {
        private readonly ChartDocDBContext _context;

        public ChiefComplaintService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<clsCC> GetChiefComplaint(string AppointmentId)
        {
            return _context.Vw_ChiefComplaint.FromSql($"SELECT PId, AppointmentId, pCcDescription FROM [Vw_ChiefComplaint] WHERE [AppointmentId]={AppointmentId}").ToList();
        }

        public string SaveChiefComplaint(clsCC cc)
        {
            //return _context.Database.ExecuteSqlCommand("USP_SaveChiefComplaint @p0, @p1", parameters: new[] { cc.PatientId, cc.Description }).ToString();
            return null;
        } 
    }
}
