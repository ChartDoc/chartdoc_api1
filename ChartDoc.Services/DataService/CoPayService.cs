﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class CoPayService : ICoPayService
    {
        private readonly ChartDocDBContext _context;

        public CoPayService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsCOPay> GetCOPay(string AppointmentID)
        {
            return _context.Vw_COPay.FromSql($"SELECT  [NAME],[REFNO1],[PatientID],[REFNO2],[PAYMENTDATE],[AMOUNT], [PaymentType], AppointmentId FROM [Vw_COPay] where AppointmentId={AppointmentID}").ToList();
        }

        public string SaveCoPay(ClsCOPay CoPay)
        {
            return _context.Database.ExecuteSqlCommand("USP_SAVE_COPAY @PatientID, @PaymentType, @REFNO1, @REFNO2, @PAYMENTDATE, @AMOUNT, @AppointmentId", 
                                                                        new SqlParameter("@PatientID", CoPay.PatientId), 
                                                                        new SqlParameter("@PaymentType", CoPay.PaymentType), 
                                                                        new SqlParameter("@REFNO1", CoPay.REFNO1), 
                                                                        new SqlParameter("@REFNO2", CoPay.REFNO2), 
                                                                        new SqlParameter("@PAYMENTDATE", CoPay.PAYMENTDATE), 
                                                                        new SqlParameter("@AMOUNT", CoPay.AMOUNT), 
                                                                        new SqlParameter("@AppointmentId", CoPay.AppointmentId)).ToString();
        }
    }
}
