﻿using ChartDoc.Context;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class FlowAreaService : IFlowAreaService
    {
        private readonly ChartDocDBContext _context;

        public FlowAreaService(ChartDocDBContext context)
        {
            _context = context;
        }

        public string UpdateFlowArea(string AppointmentID, string RoomNO, string Flowarea)
        {
            return _context.Database.ExecuteSqlCommand($"USP_UpdateFloorArea {AppointmentID}, {RoomNO}, {Flowarea}").ToString();
        }
    }
}
