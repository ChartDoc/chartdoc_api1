﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class EncounterService : IEncounterService
    {
        private readonly ChartDocDBContext _context;

        public EncounterService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsEncounter> GetEncounter(string PatientId)
        {
            return _context.Vw_Encounter_V2.FromSql($"SELECT Id, [DoctorName], [PatientID], [Patient],[EncounterDate] ,[EncounterNote],[DoctorID],[Summary] FROM [Vw_Encounter_V2] WHERE [PatientID]='" + PatientId + "' order by [EncounterDate] desc").ToList();
        }

        public string SaveEncounter(ClsEncounter encounter)
        {
            return _context.Database.ExecuteSqlCommand("USP_INSERTPATIEN_V2 @EncounterNote,@Summary,@PatientID,@DoctorID",
                                                        new SqlParameter("@EncounterNote", encounter.EncounterNote),
                                                        new SqlParameter("@Summary", encounter.Summary),
                                                        new SqlParameter("@PatientID", encounter.PatientID),
                                                        new SqlParameter("@DoctorID", encounter.DoctorID)).ToString();
        }
    }
}
