﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class ObservationService : IObservationService
    {
        private readonly ChartDocDBContext _context;

        public ObservationService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<clsObservation> GetObservation(string AppointmentId)
        {
            return _context.Vw_Observation.FromSql($"SELECT PatientID, PId, pBloodPressure_L,pBloodPressure_R,pTemperature,pHeight_L,pHeight_R,pWeight,pPulse,pRespiratory, AppointmentId FROM Vw_Observation WHERE AppointmentId={AppointmentId}").ToList();
        }

        public string SaveObservation(clsObservation objObservation)
        {
            return _context.Database.ExecuteSqlCommand("USP_UPDATE_OBSERVATION_V2 @PatientId,@BloodPressure_L,@BloodPressure_R,@Temperature,@Height_L,@Height_R,@Weight,@Pulse,@Respiratory",
                                                        new SqlParameter("@PatientId", objObservation.PatientID),
                                                        new SqlParameter("@BloodPressure_L", objObservation.pBloodPressure_L),
                                                        new SqlParameter("@BloodPressure_R", objObservation.pBloodPressure_R),
                                                        new SqlParameter("@Temperature", objObservation.pTemperature),
                                                        new SqlParameter("@Height_L", objObservation.pHeight_L),
                                                        new SqlParameter("@Height_R", objObservation.pHeight_R),
                                                        new SqlParameter("@Weight", objObservation.pWeight),
                                                        new SqlParameter("@Pulse", objObservation.pPulse),
                                                        new SqlParameter("@Respiratory", objObservation.pRespiratory)).ToString();
        }
    }
}
