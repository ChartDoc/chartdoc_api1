﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class RoleService : IRoleService
    {
        private readonly ChartDocDBContext _context;

        public RoleService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsRole> GetRole()
        {
            return _context.tblMenuList1.ToList();
        }
    }
}
