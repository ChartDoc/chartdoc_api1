﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class ServiceDetailsService : IServiceDetailsService
    {
        private readonly ChartDocDBContext _context;

        public ServiceDetailsService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsService> GetServiceDetails()
        {
            return _context.M_SERVICE.ToList();
        }
    }
}
