﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class CptService : ICptService
    {
        private readonly ChartDocDBContext _context;

        public CptService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsCPT> GetCPT()
        {
            return _context.Vw_CPT.FromSql("SELECT Id, Code, [Desc], PatientId, AppointmentId FROM Vw_CPT").ToList();
        }

        public List<ClsCPT> GetSavedCPT(string PatientId)
        {
            return _context.Vw_CPT.FromSql($"SELECT Id, Code, [Desc], PatientId, AppointmentId FROM Vw_CPT WHERE PatientId='{PatientId}'").ToList();
        }

        public string SaveCPT(int appointmentId, string cpt)
        {
            return _context.Database.ExecuteSqlCommand("USP_SAVE_CPT_V2 @PATIENTID, @CPTXML",
                                                        new SqlParameter("@PATIENTID", appointmentId),
                                                        new SqlParameter("@CPTXML", cpt)).ToString();
        }
    }
}
