﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class FlowSheetService : IFlowSheetService
    {
        private readonly ChartDocDBContext _context;

        public FlowSheetService(ChartDocDBContext context)
        {
            _context = context;
        }

        #region Get Appointment
        public List<clsFlowSheet> GetAppointment(string date)
        {
            
            var appointmentList = _context.T_ScheduleAppointment.FromSql($"USP_FETCHAPPOINTMENT_NEW {date}").ToList();

            List<clsFlowSheet> flowSheets = new List<clsFlowSheet>();
            foreach (var item in appointmentList)
            {
                if(item.FlowArea == "Appointment" || item.FlowArea == "")
                {
                    flowSheets.Add(new clsFlowSheet
                    {
                        schduleAppoinment = new clsScheduleAppointment
                        {
                            Address = item.Address,
                            AppointmentFromTime = item.AppointmentFromTime,
                            AppointmentToTime = item.AppointmentToTime,
                            AppointmentId = item.AppointmentId,
                            ColorCode = item.ColorCode,
                            Date = item.Date,
                            DateofBirth = item.DateofBirth,
                            DoctorID = item.DoctorID,
                            DoctorName = item.DoctorName,
                            Email = item.Email,
                            FlowArea = item.FlowArea,
                            FromTime = item.FromTime,
                            Gender = item.Gender,
                            IsReady = item.IsReady,
                            Note = item.Note,
                            PatientID = item.PatientID,
                            PatientName = item.PatientName,
                            Phoneno = item.Phoneno,
                            PositionID = item.PositionID,
                            Reason = item.Reason,
                            ReasonID = item.ReasonID,
                            ROOMNO = item.ROOMNO,
                            ServiceID = item.ServiceID,
                            ToTime = item.ToTime
                        },
                        WaitingArea = new clsWaitingArea
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        },
                        ConsultationRoom = new clsConsultationRoom
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        },
                        CheckingOut = new clsCheckingOut
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        }
                    });
                }
                else if (item.FlowArea == "Waiting")
                {
                    flowSheets.Add(new clsFlowSheet
                    {
                        schduleAppoinment = new clsScheduleAppointment
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        },
                        WaitingArea = new clsWaitingArea
                        {
                            Address = item.Address,
                            AppointmentFromTime = item.AppointmentFromTime,
                            AppointmentToTime = item.AppointmentToTime,
                            AppointmentId = item.AppointmentId,
                            ColorCode = item.ColorCode,
                            Date = item.Date,
                            DateofBirth = item.DateofBirth,
                            DoctorID = item.DoctorID,
                            DoctorName = item.DoctorName,
                            Email = item.Email,
                            FlowArea = item.FlowArea,
                            FromTime = item.FromTime,
                            Gender = item.Gender,
                            IsReady = item.IsReady,
                            Note = item.Note,
                            PatientID = item.PatientID,
                            PatientName = item.PatientName,
                            Phoneno = item.Phoneno,
                            PositionID = item.PositionID,
                            Reason = item.Reason,
                            ReasonID = item.ReasonID,
                            ROOMNO = item.ROOMNO,
                            ServiceID = item.ServiceID,
                            ToTime = item.ToTime
                        },
                        ConsultationRoom = new clsConsultationRoom
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        },
                        CheckingOut = new clsCheckingOut
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        }
                    });
                }
                else if (item.FlowArea == "Encounter")
                {
                    flowSheets.Add(new clsFlowSheet
                    {
                        schduleAppoinment = new clsScheduleAppointment
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        },
                        WaitingArea = new clsWaitingArea
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        },
                        ConsultationRoom = new clsConsultationRoom
                        {
                            Address = item.Address,
                            AppointmentFromTime = item.AppointmentFromTime,
                            AppointmentToTime = item.AppointmentToTime,
                            AppointmentId = item.AppointmentId,
                            ColorCode = item.ColorCode,
                            Date = item.Date,
                            DateofBirth = item.DateofBirth,
                            DoctorID = item.DoctorID,
                            DoctorName = item.DoctorName,
                            Email = item.Email,
                            FlowArea = item.FlowArea,
                            FromTime = item.FromTime,
                            Gender = item.Gender,
                            IsReady = item.IsReady,
                            Note = item.Note,
                            PatientID = item.PatientID,
                            PatientName = item.PatientName,
                            Phoneno = item.Phoneno,
                            PositionID = item.PositionID,
                            Reason = item.Reason,
                            ReasonID = item.ReasonID,
                            ROOMNO = item.ROOMNO,
                            ServiceID = item.ServiceID,
                            ToTime = item.ToTime
                        },
                        CheckingOut = new clsCheckingOut
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        }
                    });
                }
                else if (item.FlowArea == "Finish")
                {
                    flowSheets.Add(new clsFlowSheet
                    {
                        schduleAppoinment = new clsScheduleAppointment
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        },
                        WaitingArea = new clsWaitingArea
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        },
                        ConsultationRoom = new clsConsultationRoom
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = ""
                        },
                        CheckingOut = new clsCheckingOut
                        {
                            Address = item.Address,
                            AppointmentFromTime = item.AppointmentFromTime,
                            AppointmentToTime = item.AppointmentToTime,
                            AppointmentId = item.AppointmentId,
                            ColorCode = item.ColorCode,
                            Date = item.Date,
                            DateofBirth = item.DateofBirth,
                            DoctorID = item.DoctorID,
                            DoctorName = item.DoctorName,
                            Email = item.Email,
                            FlowArea = item.FlowArea,
                            FromTime = item.FromTime,
                            Gender = item.Gender,
                            IsReady = item.IsReady,
                            Note = item.Note,
                            PatientID = item.PatientID,
                            PatientName = item.PatientName,
                            Phoneno = item.Phoneno,
                            PositionID = item.PositionID,
                            Reason = item.Reason,
                            ReasonID = item.ReasonID,
                            ROOMNO = item.ROOMNO,
                            ServiceID = item.ServiceID,
                            ToTime = item.ToTime
                        }
                    });
                }
            }

            return flowSheets;
        }
        #endregion

        #region Get FlowSheet Details
        public List<clsFlowSheet> GetFlowsheet(string date)
        {
            var appointmentList = _context.T_ScheduleAppointment.FromSql($"USP_FlowSheet_NEW {date}").ToList();

            List<clsFlowSheet> flowSheets = new List<clsFlowSheet>();
            foreach (var item in appointmentList)
            {
                if (item.FlowArea == "Appointment")
                {
                    flowSheets.Add(new clsFlowSheet
                    {
                        schduleAppoinment = new clsScheduleAppointment
                        {
                            Address = item.Address,
                            AppointmentFromTime = item.AppointmentFromTime,
                            AppointmentToTime = item.AppointmentToTime,
                            AppointmentId = item.AppointmentId,
                            ColorCode = item.ColorCode,
                            Date = item.Date,
                            DateofBirth = item.DateofBirth,
                            DoctorID = item.DoctorID,
                            DoctorName = item.DoctorName,
                            Email = item.Email,
                            FlowArea = item.FlowArea,
                            FromTime = item.FromTime,
                            Gender = item.Gender,
                            IsReady = item.IsReady,
                            Note = item.Note,
                            PatientID = item.PatientID,
                            PatientName = item.PatientName,
                            Phoneno = item.Phoneno,
                            PositionID = item.PositionID,
                            Reason = item.Reason,
                            ReasonID = item.ReasonID,
                            ROOMNO = item.ROOMNO,
                            ServiceID = item.ServiceID,
                            ToTime = item.ToTime
                        },
                        WaitingArea = new clsWaitingArea
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        },
                        ConsultationRoom = new clsConsultationRoom
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        },
                        CheckingOut = new clsCheckingOut
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        }
                    });
                }
                else if (item.FlowArea == "Waiting")
                {
                    flowSheets.Add(new clsFlowSheet
                    {
                        schduleAppoinment = new clsScheduleAppointment
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        },
                        WaitingArea = new clsWaitingArea
                        {
                            Address = item.Address,
                            AppointmentFromTime = item.AppointmentFromTime,
                            AppointmentToTime = item.AppointmentToTime,
                            AppointmentId = item.AppointmentId,
                            ColorCode = item.ColorCode,
                            Date = item.Date,
                            DateofBirth = item.DateofBirth,
                            DoctorID = item.DoctorID,
                            DoctorName = item.DoctorName,
                            Email = item.Email,
                            FlowArea = item.FlowArea,
                            FromTime = item.FromTime,
                            Gender = item.Gender,
                            IsReady = item.IsReady,
                            Note = item.Note,
                            PatientID = item.PatientID,
                            PatientName = item.PatientName,
                            Phoneno = item.Phoneno,
                            PositionID = item.PositionID,
                            Reason = item.Reason,
                            ReasonID = item.ReasonID,
                            ROOMNO = item.ROOMNO,
                            ServiceID = item.ServiceID,
                            ToTime = item.ToTime
                        },
                        ConsultationRoom = new clsConsultationRoom
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        },
                        CheckingOut = new clsCheckingOut
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        }
                    });
                }
                else if (item.FlowArea == "Encounter")
                {
                    flowSheets.Add(new clsFlowSheet
                    {
                        schduleAppoinment = new clsScheduleAppointment
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        },
                        WaitingArea = new clsWaitingArea
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        },
                        ConsultationRoom = new clsConsultationRoom
                        {
                            Address = item.Address,
                            AppointmentFromTime = item.AppointmentFromTime,
                            AppointmentToTime = item.AppointmentToTime,
                            AppointmentId = item.AppointmentId,
                            ColorCode = item.ColorCode,
                            Date = item.Date,
                            DateofBirth = item.DateofBirth,
                            DoctorID = item.DoctorID,
                            DoctorName = item.DoctorName,
                            Email = item.Email,
                            FlowArea = item.FlowArea,
                            FromTime = item.FromTime,
                            Gender = item.Gender,
                            IsReady = item.IsReady,
                            Note = item.Note,
                            PatientID = item.PatientID,
                            PatientName = item.PatientName,
                            Phoneno = item.Phoneno,
                            PositionID = item.PositionID,
                            Reason = item.Reason,
                            ReasonID = item.ReasonID,
                            ROOMNO = item.ROOMNO,
                            ServiceID = item.ServiceID,
                            ToTime = item.ToTime
                        },
                        CheckingOut = new clsCheckingOut
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        }
                    });
                }
                else if (item.FlowArea == "Finish")
                {
                    flowSheets.Add(new clsFlowSheet
                    {
                        schduleAppoinment = new clsScheduleAppointment
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        },
                        WaitingArea = new clsWaitingArea
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        },
                        ConsultationRoom = new clsConsultationRoom
                        {
                            AppointmentId = 0,
                            DoctorID = "",
                            DoctorName = "",
                            FromTime = TimeSpan.Zero,
                            ToTime = TimeSpan.Zero,
                            PatientID = "",
                            PatientName = "",
                            DateofBirth = "",
                            Gender = "",
                            Phoneno = "",
                            Email = "",
                            Address = "",
                            Note = "",
                            PositionID = "",
                            ReasonID = "",
                            Reason = "",
                            AppointmentFromTime = "",
                            AppointmentToTime = "",
                            FlowArea = ""
                        },
                        CheckingOut = new clsCheckingOut
                        {
                            Address = item.Address,
                            AppointmentFromTime = item.AppointmentFromTime,
                            AppointmentToTime = item.AppointmentToTime,
                            AppointmentId = item.AppointmentId,
                            ColorCode = item.ColorCode,
                            Date = item.Date,
                            DateofBirth = item.DateofBirth,
                            DoctorID = item.DoctorID,
                            DoctorName = item.DoctorName,
                            Email = item.Email,
                            FlowArea = item.FlowArea,
                            FromTime = item.FromTime,
                            Gender = item.Gender,
                            IsReady = item.IsReady,
                            Note = item.Note,
                            PatientID = item.PatientID,
                            PatientName = item.PatientName,
                            Phoneno = item.Phoneno,
                            PositionID = item.PositionID,
                            Reason = item.Reason,
                            ReasonID = item.ReasonID,
                            ROOMNO = item.ROOMNO,
                            ServiceID = item.ServiceID,
                            ToTime = item.ToTime
                        }
                    });
                }
            }

            return flowSheets;
        }
        #endregion
    }
}
