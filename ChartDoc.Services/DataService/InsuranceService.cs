﻿using ChartDoc.Context;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class InsuranceService : IInsuranceService
    {
        private readonly ChartDocDBContext _context;

        public InsuranceService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<string> GetInsurance(string PatientID)
        {
            var list = _context.T_Patient_Insurance.FromSql($"USP_fetch_Insurance {PatientID}").ToList();
            List<string> val = list.Select(x => x.Desc).ToList();
            return val;
        }
    }
}
