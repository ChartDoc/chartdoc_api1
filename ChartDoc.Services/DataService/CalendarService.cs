﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class CalendarService : ICalendarService
    {
        private readonly ChartDocDBContext _context;

        public CalendarService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsCalender> GetCalender()
        {
            return _context.T_OfficeCalander.FromSql($"USP_BindCalender").ToList();
        }

        public string SaveCalender(ClsCalender Calendr)
        {
            return _context.Database.ExecuteSqlCommand("USP_OfficeCalander @ID, @TAG, @DoctorID, @Date, @FromTime, @ToTime, @EventReason, @BooingTag",
                                                                            new SqlParameter("@ID", Calendr.Id),
                                                                            new SqlParameter("@TAG", Calendr.TAG),
                                                                            new SqlParameter("@DoctorID", Calendr.DoctorID),
                                                                            new SqlParameter("@Date", Calendr.Date),
                                                                            new SqlParameter("@FromTime", Calendr.FromTime),
                                                                            new SqlParameter("@ToTime", Calendr.ToTime),
                                                                            new SqlParameter("@EventReason", Calendr.EventReason),
                                                                            new SqlParameter("@BooingTag", Calendr.BooingTag)).ToString();
        }
    }
}
