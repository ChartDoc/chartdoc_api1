﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class ImmunizationService : IImmunizationService
    {
        private readonly ChartDocDBContext _context;

        public ImmunizationService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<ClsImmunizations> GetImmunizations(string PatientId)
        {
            return _context.t_Immunizations.Where(x => x.PatientId == PatientId).ToList();
        }
    }
}
