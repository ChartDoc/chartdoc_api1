﻿using ChartDoc.Context;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChartDoc.Services.DataService
{
    public class ImpressionPlanService : IImpressionPlanService
    {
        private readonly ChartDocDBContext _context;

        public ImpressionPlanService(ChartDocDBContext context)
        {
            _context = context;
        }

        public List<clsImpression> GetImpressionPlan(string AppointmentId)
        {
            return _context.T_IMPRESSION.FromSql($"SELECT [ID], [PATIENTID],  [IMPRESSION] AS Description FROM [T_IMPRESSION] WHERE [AppointmentID]={AppointmentId}").ToList();
        }

        public string SaveImpressionPlan(clsImpression impression)
        {
            return _context.Database.ExecuteSqlCommand("USP_SaveImpressionPlan @patientId, @description", impression.patientId, impression.Description ).ToString();
        }
    }
}
