﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ChartDoc.Models;
using ChartDoc.Services.Infrastructure;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ChartDoc.Api.Controllers
{
    [Route("api/ChartDoc")]
    [EnableCors("AllowMyOrigin")]
    [ApiController]
    public class ChartDocController : ControllerBase
    {
        #region Declaration of variables
        private readonly IUserService _userService;
        private readonly IIcdService _icdService;
        private readonly IPatientDetailsService _patientDetailsService;
        private readonly IDiagnosisService _diagnosisService;
        private readonly IProcedureService _procedureService;
        private readonly IDepartmentService _departmentService;
        private readonly IInsuranceService _insuranceService;
        private readonly ICalendarService _calendarService;
        private readonly IReasonService _reasonService;
        private readonly ICoPayService _copayService;
        private readonly IFlowSheetService _flowSheetService;
        private readonly ICptService _cptService;
        private readonly IEncounterService _encounterService;
        private readonly IRoleService _roleService;
        private readonly IUserTypeService _userTypeService;
        private readonly IOthersPopulateService _othersPopulateService;
        private readonly IPatientInformationService _patientInformationService;
        private readonly IServiceDetailsService _serviceDetailsService;
        private readonly IImpressionPlanService _impressionPlanService;
        private readonly IDocumentService _documentService;
        private readonly IChiefComplaintService _chiefComplaintService;
        private readonly IFollowupService _followupService;
        private readonly IObservationService _observationService;
        private readonly ISharedService _sharedService;
        private readonly IAllergiesService _allergiesService;
        private readonly IImmunizationService _immunizationService;
        private readonly ISocialService _socialService;
        private readonly IFamilyService _familyService;
        private readonly ICheckOutService _checkOutService;
        private readonly IScheduleAppointmentService _scheduleAppointmentService;
        private readonly IFlowAreaService _floorAreaService;
        private readonly IAppointmentService _appointmentService;
        private IHostingEnvironment _environment;
        private readonly IAllergyImmunizationAlertSocialFamilyService _allergyImmunizationAlertSocialFamilyService;
        #endregion

        #region Constructor
        public ChartDocController(IUserService userService, 
            IIcdService icdService,
            IPatientDetailsService patientDetailsService,
            IDiagnosisService diagnosisService,
            IProcedureService procedureService,
            IDepartmentService departmentService,
            IInsuranceService insuranceService,
            ICalendarService calendarService,
            IReasonService reasonService,
            ICoPayService coPayService,
            IFlowSheetService flowSheetService,
            ICptService cptService,
            IEncounterService encounterService,
            IRoleService roleService,
            IUserTypeService userTypeService,
            IOthersPopulateService othersPopulateService,
            IPatientInformationService patientInformationService,
            IServiceDetailsService serviceDetailsService,
            IImpressionPlanService impressionPlanService,
            IDocumentService documentService,
            IChiefComplaintService chiefComplaintService,
            IFollowupService followupService,
            IObservationService observationService,
            ISharedService sharedService,
            IAllergiesService allergiesService,
            IImmunizationService immunizationService,
            ISocialService socialService,
            IFamilyService familyService,
            ICheckOutService checkOutService,
            IScheduleAppointmentService scheduleAppointmentService,
            IFlowAreaService floorAreaService,
            IAppointmentService appointmentService,
            IHostingEnvironment environment,
            IAllergyImmunizationAlertSocialFamilyService allergyImmunizationAlertSocialFamilyService)
        {
            _userService = userService;
            _icdService = icdService;
            _patientDetailsService = patientDetailsService;
            _diagnosisService = diagnosisService;
            _procedureService = procedureService;
            _departmentService = departmentService;
            _insuranceService = insuranceService;
            _calendarService = calendarService;
            _reasonService = reasonService;
            _copayService = coPayService;
            _flowSheetService = flowSheetService;
            _cptService = cptService;
            _encounterService = encounterService;
            _roleService = roleService;
            _userTypeService = userTypeService;
            _othersPopulateService = othersPopulateService;
            _patientInformationService = patientInformationService;
            _serviceDetailsService = serviceDetailsService;
            _impressionPlanService = impressionPlanService;
            _documentService = documentService;
            _chiefComplaintService = chiefComplaintService;
            _followupService = followupService;
            _observationService = observationService;
            _sharedService = sharedService;
            _allergiesService = allergiesService;
            _immunizationService = immunizationService;
            _socialService = socialService;
            _familyService = familyService;

            _checkOutService = checkOutService;
            _scheduleAppointmentService = scheduleAppointmentService;
            _floorAreaService = floorAreaService;
            _appointmentService = appointmentService;
            _environment = environment;
            _allergyImmunizationAlertSocialFamilyService = allergyImmunizationAlertSocialFamilyService;
        }
        #endregion

        #region GET Api
        #region Get Login Status
        [HttpGet]
        [Route("GetLoginStatus/{userName}/{password}")]
        public ActionResult<clsUser> GetLoginStatus(string userName, string password)
        {
            return _userService.GetUser(userName, password);
        }
        #endregion

        #region Get ICD Details
        [HttpGet]
        [Route("GetICD")]
        public ActionResult<IEnumerable<clsICD>> GetICD()
        {
            return _icdService.GetAllICD();
        }

        [HttpGet]
        [Route("GetSavedICD/{PatientId}")]
        public ActionResult<IEnumerable<clsICD>> GetSavedICD(string PatientId)
        {
            return _icdService.GetSavedICD(PatientId);
        }
        #endregion

        #region Search Patient
        [HttpGet]
        [Route("SearchPatient")]
        public ActionResult<IEnumerable<clsPatientDetails>> SearchPatient(string firstName = "", string lastName = "", string DOB = null, string mobNo = "", string emailId = "")
        {
            return _patientDetailsService.SearchPatient(firstName, lastName, DOB, mobNo, emailId);
        }
        #endregion

        #region Get All Patients Details
        [HttpGet]
        [Route("GetAllPatients")]
        public ActionResult<IEnumerable<clsPatientDetails>> GetAllPatients()
        {
            return _patientDetailsService.GetAllPatients();
        }
        #endregion

        #region Get Patient Diagnosis Details
        [HttpGet]
        [Route("GetDiagnosisByPatientId/{patientId}")]
        public ActionResult<IEnumerable<clsDiagnosis>> GetDiagnosisByPatientId(string patientId)
        {
            return _diagnosisService.GetDiagnosisByPatientId(patientId);
        }
        #endregion

        #region Get Patient Procedure Details
        [HttpGet]
        [Route("GetProceduresByPatientId/{patientId}")]
        public ActionResult<IEnumerable<clsProcedures>> GetProceduresByPatientId(string patientId)
        {
            return _procedureService.GetProcedureByPatientId(patientId);
        }
        #endregion

        #region Get All Departments
        [HttpGet]
        [Route("GetDept")]
        public ActionResult<IEnumerable<clsDept>> GetDept()
        {
            return _departmentService.GetDept();
        }
        #endregion

        #region Get Insurance By PatientId
        [HttpGet]
        [Route("GetInsurance/{PatientID}")]
        public ActionResult<IEnumerable<string>> GetInsurance(string PatientID)
        {
            return _insuranceService.GetInsurance(PatientID);
        }
        #endregion

        #region Get Doctors List
        [HttpGet]
        [Route("GetDoctorList/{date}")]
        public ActionResult<IEnumerable<clsUser>> GetDoctorList(string date)
        {
            return _userService.GetAllDoctorsDetails(date);
        }
        #endregion

        #region Get User List
        [HttpGet]
        [Route("GetUserList")]
        public ActionResult<IEnumerable<ClsDoctor>> GetUserList(string userType="")
        {
            return _userService.GetUserList(userType);
        }
        #endregion

        #region Get Calendar Details
        [HttpGet]
        [Route("GetCalender")]
        public ActionResult<IEnumerable<ClsCalender>> GetCalender()
        {
            return _calendarService.GetCalender();
        }
        #endregion

        #region Get Reason
        [HttpGet]
        [Route("GetReason/{Type}")]
        public ActionResult<IEnumerable<ClsReason>> GetReason(string Type)
        {
            return _reasonService.GetReason(Type);
        }
        #endregion

        #region Get CoPay
        [HttpGet]
        [Route("GetCOPay/{AppointmentID}")]
        public ActionResult<IEnumerable<ClsCOPay>> GetCOPay(string AppointmentID)
        {
            return _copayService.GetCOPay(AppointmentID);
        }
        #endregion

        #region Get Appointment
        [HttpGet]
        [Route("GetAppointment/{date}")]
        public ActionResult<IEnumerable<clsFlowSheet>> GetAppointment(string date)
        {
            return _flowSheetService.GetAppointment(date);
        }
        #endregion

        #region Get CPT Details
        [HttpGet]
        [Route("GetCPT")]
        public ActionResult<IEnumerable<ClsCPT>> GetCPT()
        {
            return _cptService.GetCPT();
        }

        [HttpGet]
        [Route("GetSavedCPT/{PatientId}")]
        public ActionResult<IEnumerable<ClsCPT>> GetSavedCPT(string PatientId)
        {
            return _cptService.GetSavedCPT(PatientId);
        }
        #endregion

        #region Get Encounter By PatientID
        [HttpGet]
        [Route("GetEncounter/{PatientId}")]
        public ActionResult<IEnumerable<ClsEncounter>> GetEncounter(string PatientId)
        {
            PatientId = _sharedService.DecodeString(PatientId);
            return _encounterService.GetEncounter(PatientId);

        }
        #endregion

        #region Get All Roles
        [HttpGet]
        [Route("GetRole")]
        public ActionResult<IEnumerable<ClsRole>> GetRole()
        {
            return _roleService.GetRole();
        }
        #endregion

        #region Get User Type By Id
        [HttpGet]
        [Route("GetUType/{Id}")]
        public ActionResult<IEnumerable<ClsUType>> GetUType(string Id)
        {
            return _userTypeService.GetUType(Id);
        }
        #endregion

        #region Get Others By Type Id
        [HttpGet]
        [Route("OtherPopulate/{Id}")]
        public ActionResult<IEnumerable<ClsOtherPopulate>> OtherPopulate(string Id)
        {
            return _othersPopulateService.OtherPopulate(Id);
        }
        #endregion

        #region Get Patient Details By Patient Id
        [HttpGet]
        [Route("GetPatientInfo/{PatientId}")]
        public ActionResult<clsPatientInformations> GetPatientInfo(string PatientId)
        {
            return _patientInformationService.GetPatientInfo(PatientId);
        }
        #endregion

        #region Get Service Details
        [HttpGet]
        [Route("GetServiceDetails")]
        public ActionResult<IEnumerable<ClsService>> GetServiceDetails()
        {
            return _serviceDetailsService.GetServiceDetails();
        }
        #endregion

        #region Get FlowSheet Details
        [HttpGet]
        [Route("GetFlowsheet/{date}")]
        public ActionResult<IEnumerable<clsFlowSheet>> GetFlowsheet(string date)
        {
            return _flowSheetService.GetFlowsheet(date);
        }
        #endregion

        #region Get Patient By Patient Id
        [HttpGet]
        [Route("SearchPatientbyID/{ID}")]
        public clsPatientDetails SearchPatientbyID(string ID)
        {
            return _patientDetailsService.SearchPatientbyID(ID);
        }
        #endregion

        #region Get Impression Plan
        [HttpGet]
        [Route("GetImpressionPlan/{AppointmentId}")]
        public ActionResult<IEnumerable<clsImpression>> GetImpressionPlan(string AppointmentId)
        {
            return _impressionPlanService.GetImpressionPlan(AppointmentId);
        }
        #endregion

        #region Get Document Details By PatientId
        [HttpGet]
        [Route("GetDocument/{PatientId}")]
        public ActionResult<IEnumerable<ClsDocument>> GetDocument(string PatientId)
        {
            return _documentService.GetDocument(PatientId);
        }
        #endregion

        #region Get Chief Complaint By Appointment Id
        [HttpGet]
        [Route("GetChiefComplaint/{AppointmentId}")]
        public ActionResult<IEnumerable<clsCC>> GetChiefComplaint(string AppointmentId)
        {
            return _chiefComplaintService.GetChiefComplaint(AppointmentId);
        }
        #endregion

        #region Get Follow Up By Appointment Id
        [HttpGet]
        [Route("GetFollowUp/{AppointmentId}")]
        public ActionResult<IEnumerable<clsFollowUp>> GetFollowUp(string AppointmentId)
        {
            return _followupService.GetFollowUp(AppointmentId);
        }
        #endregion

        #region Get Observation By Appointment Id
        [HttpGet]
        [Route("GetObservation/{AppointmentId}")]
        public ActionResult<IEnumerable<clsObservation>> GetObservation(string AppointmentId)
        {
            return _observationService.GetObservation(AppointmentId);
        }
        #endregion

        #region Get Allergies By Patient Id
        [HttpGet]
        [Route("GetAllergies/{PatientId}")]
        public ActionResult<IEnumerable<ClsAllergies>> GetAllergies(string PatientId)
        {
            PatientId = _sharedService.DecodeString(PatientId);
            return _allergiesService.GetAllergies(PatientId);
        }
        #endregion

        #region Get Immunization By Patient Id
        [HttpGet]
        [Route("GetImmunizations/{PatientId}")]
        public ActionResult<IEnumerable<ClsImmunizations>> GetImmunizations(string PatientId)
        {
            PatientId = _sharedService.DecodeString(PatientId);
            return _immunizationService.GetImmunizations(PatientId);
        }
        #endregion

        #region Get Social Information By Patient Id
        [HttpGet]
        [Route("GetSocials/{PatientId}")]
        public ActionResult<IEnumerable<ClsSocials>> GetSocials(string PatientId)
        {
            PatientId = _sharedService.DecodeString(PatientId);
            return _socialService.GetSocials(PatientId);
        }
        #endregion

        #region Get Families History By Patient Id
        [HttpGet]
        [Route("GetFamilies/{PatientId}")]
        public List<ClsFamilies> GetFamilies(string PatientId)
        {
            PatientId = _sharedService.DecodeString(PatientId);
            return _familyService.GetFamilies(PatientId);
        }
        #endregion
        #endregion

        #region PUT Api
        #region Update CheckOut Status
        [HttpPut]
        [Route("UpdateCheckOutStatus/{AppointmentID}/{ReasonCode}")]
        public ActionResult<string> UpdateCheckOutStatus(string AppointmentID, string ReasonCode)
        {
            return _checkOutService.UpdateCheckOut(AppointmentID, ReasonCode);
        }
        #endregion

        #region Update Mark Ready
        [HttpPut]
        [Route("UpdateMarkReady/{AppointmentID}/{Flag}")]
        public string UpdateMarkReady(string AppointmentID, string Flag)
        {
            return _scheduleAppointmentService.UpdateMarkReady(AppointmentID, Flag);
        }
        #endregion

        #region Update Flow Area
        [HttpPut]
        [Route("UpdateFloorArea/{AppointmentID}/{RoomNO}/{Flowarea}")]
        public string UpdateFlowArea(string AppointmentID, string RoomNO, string Flowarea)
        {
            return _floorAreaService.UpdateFlowArea(AppointmentID, RoomNO, Flowarea);
        }
        #endregion
        #endregion

        #region POST Api
        #region Save CoPay
        [HttpPost]
        [Route("SaveCoPay")]
        public string SaveCoPay([FromBody] ClsCOPay CoPay)
        {
            return _copayService.SaveCoPay(CoPay);
        }
        #endregion

        #region Save Calendar
        [HttpPost]
        [Route("SaveCalender")]
        public string SaveCalender([FromBody] ClsCalender Calendr)
        {
            return _calendarService.SaveCalender(Calendr);
        }
        #endregion

        #region Save Appointment Details
        [HttpPost]
        [Route("SaveAppointment")]
        public ActionResult<string> SaveAppointment([FromBody] clsAppointment appointment)
        {
            return _appointmentService.SaveActiveAppointment(appointment);
        }

        [HttpPost]
        [Route("SaveAppointmentNew")]
        public string SaveAppointmentNew(IFormCollection data, IFormFile uploadFile)
        {
            clsAppointment appointment = JsonConvert.DeserializeObject<clsAppointment>(data["appointmentDetails"]);
            if (uploadFile.Length > 0)
            {
                var uploads = Path.Combine(_environment.WebRootPath, "Images/Appointment");
                Guid FileName = Guid.NewGuid();
                string filePath = Path.Combine(uploads, FileName.ToString() + "-" + appointment.AppointmentNo);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    try
                    {
                        uploadFile.CopyToAsync(fileStream);
                        appointment.FilePath = filePath;
                    }
                    catch(Exception ex)
                    {

                    }
                    
                }
            }
            return _appointmentService.SaveActiveAppointment(appointment);
        }
        #endregion

        #region Save Chief Complaint
        [HttpPost]
        [Route("SaveChiefComplaint")]
        public ActionResult<string> SaveChiefComplaint([FromBody] clsCC cc)
        {
            return _chiefComplaintService.SaveChiefComplaint(cc);
        }
        #endregion

        #region Save Impression Plan
        [HttpPost]
        [Route("SaveImpressionPlan")]
        public ActionResult<string> SaveImpressionPlan([FromBody] clsImpression impression)
        {
            return _impressionPlanService.SaveImpressionPlan(impression);
        }
        #endregion

        #region Save User
        [HttpPost]
        [Route("SaveUser")]
        public string SaveUser([FromBody] ClsUserObj iUser)
        {
            string xmlUser = null;
            DataTable dt = new DataTable();

            dt = _sharedService.SingleObjToDataTable<ClsUserObj>(iUser);
            xmlUser = _sharedService.ConvertDatatableToXML(dt);
            return _userService.SaveUser(xmlUser);
        }

        [HttpPost]
        [Route("SaveUserNew")]
        public string SaveUserNew(IFormCollection data, IFormFile uploadFile)
        {
            string xmlUser = null;
            DataTable dt = new DataTable();

            ClsUserObj iUser = JsonConvert.DeserializeObject<ClsUserObj>(data["userDetails"]);
            if (uploadFile.Length > 0)
            {
                var uploads = Path.Combine(_environment.WebRootPath, "Images/User");
                Guid FileName = Guid.NewGuid();
                string filePath = Path.Combine(uploads, FileName.ToString() + "-" + iUser.FULLName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    try
                    {
                        uploadFile.CopyToAsync(fileStream);
                        iUser.image = filePath;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            dt = _sharedService.SingleObjToDataTable<ClsUserObj>(iUser);
            xmlUser = _sharedService.ConvertDatatableToXML(dt);
            return _userService.SaveUser(xmlUser);
        }
        #endregion

        #region Save ICD
        [HttpPost]
        [Route("SaveICD")]
        public string SaveICD([FromBody] clsICD[] ICD)
        {
            string xmlICD = null;
            DataTable dt = new DataTable();

            dt = _sharedService.ObjArrayToDataTable<clsICD>(ICD);
            xmlICD = _sharedService.ConvertDatatableToXML(dt);

            return _icdService.SaveICD(ICD[0].AppointmentId, xmlICD);
        }
        #endregion

        #region Save Observation
        [HttpPost]
        [Route("SaveObservation")]
        public string SaveObservation([FromBody] clsObservation objObservation)
        {
            return _observationService.SaveObservation(objObservation);
        }
        #endregion

        #region Save Allergies, Immunizations, Alert, Social and Family History
        [HttpPost]
        [Route("SaveAllergyImmunization")]
        public string SaveAllergyImmunization([FromBody] ClsAllergyImmunization clsAllergyImmunization)
        {
            string xmlAllergy = null;
            string xmlImmunization = null;
            string xmlAlert = null;
            string xmlFamily = null;
            string xmlSocial = null;

            DataTable dtAlert = new DataTable();
            DataTable dtAllergy = new DataTable();
            DataTable dtImmunization = new DataTable();
            DataTable dtFamily = new DataTable();
            DataTable dtSocial = new DataTable();

            dtAllergy = _sharedService.ObjArrayToDataTable<ClsAllergies>(clsAllergyImmunization.Allergies);
            xmlAllergy = _sharedService.ConvertDatatableToXML(dtAllergy);

            dtImmunization = _sharedService.ObjArrayToDataTable<ClsImmunizations>(clsAllergyImmunization.Immunizations);
            xmlImmunization = _sharedService.ConvertDatatableToXML(dtImmunization);

            dtAlert = _sharedService.SingleObjToDataTable<ClsAlert>(clsAllergyImmunization.Alert);
            xmlAlert = _sharedService.ConvertDatatableToXML(dtAlert);

            dtFamily = _sharedService.ObjArrayToDataTable<ClsFamilies>(clsAllergyImmunization.Families);
            xmlFamily = _sharedService.ConvertDatatableToXML(dtFamily);

            dtSocial = _sharedService.ObjArrayToDataTable<ClsSocials>(clsAllergyImmunization.Socials);
            xmlSocial = _sharedService.ConvertDatatableToXML(dtSocial);

            return _allergyImmunizationAlertSocialFamilyService.SaveAllergyImmunization(clsAllergyImmunization.PatientID, xmlAllergy, xmlImmunization, xmlAlert, xmlFamily, xmlSocial);
        }
        #endregion

        #region Save Patient Details
        [HttpPost]
        [Route("CreatePatient")]
        public string CreatePatient([FromBody] clsPatientInformations Ptn)
        {
            string @PatientID = "", @p_Details = "", @p_Billing = "", @p_EmergencyContact = "", @p_EmployerContact = "";
            string @p_Insurance = "", @p_Social = "", @p_Authorization = "";

            DataTable dt = new DataTable();
            @PatientID = Ptn.sPatientDetails.PatientId;

            dt = _sharedService.SingleObjToDataTable<clsPatientDetails>(Ptn.sPatientDetails);
            @p_Details = _sharedService.ConvertDatatableToXML(dt);

            dt = _sharedService.SingleObjToDataTable<ClsPatientBilling>(Ptn.sPatientBilling);
            @p_Billing = _sharedService.ConvertDatatableToXML(dt);

            dt = _sharedService.SingleObjToDataTable<ClsPatientEmergencyContact>(Ptn.sPatientEmergency);
            @p_EmergencyContact = _sharedService.ConvertDatatableToXML(dt);

            dt = _sharedService.SingleObjToDataTable<ClsPatientEmployerContact>(Ptn.sPatientEmpContact);
            @p_EmployerContact = _sharedService.ConvertDatatableToXML(dt);

            dt = _sharedService.ObjArrayToDataTable<ClsPatientInsurance>(Ptn.sPatientInsurance);
            @p_Insurance = _sharedService.ConvertDatatableToXML(dt);

            dt = _sharedService.SingleObjToDataTable<ClsPatientSocial>(Ptn.sPatientSocial);
            @p_Social = _sharedService.ConvertDatatableToXML(dt);

            dt = _sharedService.SingleObjToDataTable<ClsPatientAuthorization>(Ptn.sPatientAuthorisation);
            @p_Authorization = _sharedService.ConvertDatatableToXML(dt);

            return _patientInformationService.CreatePatient(PatientID, p_Details, p_Billing, p_EmergencyContact, p_EmployerContact, p_EmployerContact, p_Insurance, p_Authorization);
        }
        #endregion

        #region Save CPT
        [HttpPost]
        [Route("SaveCPT")]
        public string SaveCPT([FromBody] ClsCPT[] CPT)
        {
            string xmlCPT = null;
            DataTable dt = new DataTable();

            dt = _sharedService.ObjArrayToDataTable<ClsCPT>(CPT);
            xmlCPT = _sharedService.ConvertDatatableToXML(dt);

            return _cptService.SaveCPT(CPT[0].AppointmentId, xmlCPT);
        }
        #endregion

        #region Save Followup
        [HttpPost]
        [Route("SaveFollowup")]
        public string SaveFollowup([FromBody] clsFollowUp objFollowup)
        {
            return _followupService.SaveFollowup(objFollowup);
        }
        #endregion

        #region Save Encounter
        [HttpPost]
        [Route("SaveEncounter")]
        public string SaveEncounter([FromBody] ClsEncounter encounter)
        {
            return _encounterService.SaveEncounter(encounter);
        }
        #endregion
        #endregion
    }
}