﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChartDoc.Api.Options;
using ChartDoc.Context;
using ChartDoc.Services.DataService;
using ChartDoc.Services.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace ChartDoc.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IIcdService, IcdService>();
            services.AddTransient<IPatientDetailsService, PatientDetailsService>();
            services.AddTransient<IDiagnosisService, DiagnosisService>();
            services.AddTransient<IProcedureService, ProcedureService>();
            services.AddTransient<IDepartmentService, DepartmentService>();
            services.AddTransient<IInsuranceService, InsuranceService>();
            services.AddTransient<ICalendarService, CalendarService>();
            services.AddTransient<IReasonService, ReasonService>();
            services.AddTransient<ICoPayService, CoPayService>();
            services.AddTransient<IFlowSheetService, FlowSheetService>();
            services.AddTransient<ICptService, CptService>();
            services.AddTransient<IEncounterService, EncounterService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IUserTypeService, UserTypeService>();
            services.AddTransient<IOthersPopulateService, OthersPopulateService>();
            services.AddTransient<IPatientInformationService, PatientInformationService>();
            services.AddTransient<IServiceDetailsService, ServiceDetailsService>();
            services.AddTransient<IImpressionPlanService, ImpressionPlanService>();
            services.AddTransient<IDocumentService, DocumentService>();
            services.AddTransient<IChiefComplaintService, ChiefComplaintService>();
            services.AddTransient<IFollowupService, FollowupService>();
            services.AddTransient<IObservationService, ObservationService>();
            services.AddTransient<ISharedService, SharedService>();
            services.AddTransient<IAllergiesService, AllergiesService>();
            services.AddTransient<IImmunizationService, ImmunizationService>();
            services.AddTransient<ISocialService, SocialService>();
            services.AddTransient<IFamilyService, FamilyService>();
            services.AddTransient<ICheckOutService, CheckOutService>();
            services.AddTransient<IScheduleAppointmentService, ScheduleAppointmentService>();
            services.AddTransient<IFlowAreaService, FlowAreaService>();
            services.AddTransient<IAppointmentService, AppointmentService>();
            services.AddTransient<IAllergyImmunizationAlertSocialFamilyService, AllergyImmunizationAlertSocialFamilyService>();

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ChartDocDBContext>
                (options => options.UseSqlServer(Configuration.GetConnectionString("constring")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ChartDoc Api", Version = "v1" });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyOrigin",
                builder => builder.AllowAnyOrigin());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            var swaggerOptions = new SwaggerOptions();
            Configuration.GetSection(nameof(SwaggerOptions)).Bind(swaggerOptions);

            app.UseSwagger(option =>
            {
                option.RouteTemplate = swaggerOptions.JsonRoute;
            });

            app.UseSwaggerUI(option =>
            {
                option.SwaggerEndpoint(swaggerOptions.UIEndpoint, swaggerOptions.Description);
            });

            app.UseHttpsRedirection();
            app.UseCors("AllowMyOrigin");
            app.UseMvc();
        }
    }
}
